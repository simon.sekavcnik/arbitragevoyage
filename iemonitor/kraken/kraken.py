from kraken_wsclient_py import kraken_wsclient_py as client
from kraken_exchange import kraken_exchange

pairs = ['ADA/CAD', 'ADA/ETH', 'ADA/EUR', 'ADA/USD', 'ADA/XBT', 'BCH/EUR', 'BCH/USD',
    'BCH/XBT', 'BSV/EUR', 'BSV/USD', 'BSV/XBT', 'DASH/EUR', 'DASH/USD', 'DASH/XBT',
    'EOS/ETH', 'EOS/EUR', 'EOS/USD', 'EOS/XBT', 'GNO/ETH', 'GNO/EUR', 'GNO/USD',
    'GNO/XBT', 'QTUM/CAD', 'QTUM/ETH', 'QTUM/EUR', 'QTUM/USD', 'QTUM/XBT', 'USDT/USD',
    'ETC/ETH', 'ETC/XBT', 'ETC/EUR', 'ETC/USD', 'ETH/XBT', 'ETH/CAD', 'ETH/EUR',
    'ETH/GBP', 'ETH/JPY', 'ETH/USD', 'LTC/XBT', 'LTC/EUR', 'LTC/USD', 'MLN/ETH',
    'MLN/XBT', 'REP/ETH', 'REP/XBT', 'REP/EUR', 'REP/USD', 'STR/EUR', 'STR/USD',
    'XBT/CAD', 'XBT/EUR', 'XBT/GBP', 'XBT/JPY', 'XBT/USD', 'BTC/CAD', 'BTC/EUR',
    'BTC/GBP', 'BTC/JPY', 'BTC/USD', 'XDG/XBT', 'XLM/XBT', 'DOGE/XBT', 'STR/XBT',
    'XLM/EUR', 'XLM/USD', 'XMR/XBT', 'XMR/EUR', 'XMR/USD', 'XRP/XBT', 'XRP/CAD',
    'XRP/EUR', 'XRP/JPY', 'XRP/USD', 'ZEC/XBT', 'ZEC/EUR', 'ZEC/JPY', 'ZEC/USD',
    'XTZ/CAD', 'XTZ/ETH', 'XTZ/EUR', 'XTZ/USD', 'XTZ/XBT']

print("Initiating kraken exchange")
ke = kraken_exchange()
ke.loadGraph(pairs, 'data/kraken_graph','data/kraken_cycles','data/kraken_triplets')
print("Initiating kraken websocket connection")
print("Don't trade by ")
kc = client.WssClient()

print("Subscribint to ticker")
kc.subscribe_public(
    subscription={
        'name':'ticker'
    },
    pair=pairs,
    callback=ke.tickerUpdate
)


print("Subscribing to order book")
kc.subscribe_public(
    subscription={
        'name':'book'
    },
    pair=pairs,#['XBT/EUR'],#pairs,
    callback=ke.orderBooksUpdate
)

kc.start()
