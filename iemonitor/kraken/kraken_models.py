import numpy as np

#List of all pairs traded on kraken
pairs = ['ADA/CAD', 'ADA/ETH', 'ADA/EUR', 'ADA/USD', 'ADA/XBT', 'BCH/EUR', 'BCH/USD',
    'BCH/XBT', 'BSV/EUR', 'BSV/USD', 'BSV/XBT', 'DASH/EUR', 'DASH/USD', 'DASH/XBT',
    'EOS/ETH', 'EOS/EUR', 'EOS/USD', 'EOS/XBT', 'GNO/ETH', 'GNO/EUR', 'GNO/USD',
    'GNO/XBT', 'QTUM/CAD', 'QTUM/ETH', 'QTUM/EUR', 'QTUM/USD', 'QTUM/XBT', 'USDT/USD',
    'ETC/ETH', 'ETC/XBT', 'ETC/EUR', 'ETC/USD', 'ETH/XBT', 'ETH/CAD', 'ETH/EUR',
    'ETH/GBP', 'ETH/JPY', 'ETH/USD', 'LTC/XBT', 'LTC/EUR', 'LTC/USD', 'MLN/ETH',
    'MLN/XBT', 'REP/ETH', 'REP/XBT', 'REP/EUR', 'REP/USD', 'STR/EUR', 'STR/USD',
    'XBT/CAD', 'XBT/EUR', 'XBT/GBP', 'XBT/JPY', 'XBT/USD', 'BTC/CAD', 'BTC/EUR',
    'BTC/GBP', 'BTC/JPY', 'BTC/USD', 'XDG/XBT', 'XLM/XBT', 'DOGE/XBT', 'STR/XBT',
    'XLM/EUR', 'XLM/USD', 'XMR/XBT', 'XMR/EUR', 'XMR/USD', 'XRP/XBT', 'XRP/CAD',
    'XRP/EUR', 'XRP/JPY', 'XRP/USD', 'ZEC/XBT', 'ZEC/EUR', 'ZEC/JPY', 'ZEC/USD',
    'XTZ/CAD', 'XTZ/ETH', 'XTZ/EUR', 'XTZ/USD', 'XTZ/XBT']

class pair_model:
    def __init__(self, pair, channelID):
        self.pair = pair
        self.channelID = channelID
        self.bestsBid = 0
        self.price = 0
        self.bestAsk = 0

    def updateRate(self, bestBid=0, bestAsk=0):
        if(bestBid != 0):
            self.bestBid = bestBid
        if(bestAsk != 0):
            self.bestAsk = bestAsk

        if(self.bestAsk>0 and self.bestBid>0):
            print(self.pair + " is active")
            self.price = (bestBid+bestAsk)/2
            print(self.price)


def matprint(mat, fmt="g"):
    col_maxes = [max([len(("{:"+fmt+"}").format(x)) for x in col]) for col in mat.T]
    for x in mat:
        for i, y in enumerate(x):
            print(("{:"+str(col_maxes[i])+fmt+"}").format(y), end="  ")
        print("")


class exchange:
    pairs = []
    currencies = []
    matrix = None

    def __init__(self):
        for pair in pairs:
            for currency in pair.split('/'):
                if currency not in self.currencies:
                    self.currencies.append(currency)
        print(self.currencies)
        print(len(self.currencies))
        self.matrix = np.zeros((len(self.currencies),len(self.currencies)))
        for i in range(len(self.currencies)):
            self.matrix[i][i]=1
        print(self.matrix)

    def addPair(self, pair, channelID):
        pair = pair_model(pair,channelID)
        if pair not in pairs:
            self.pairs.append(pair)


    def updatePair(self, channelID, bestBid=0, bestAsk=0):
        index = next((i for i, item in enumerate(self.pairs) if item.channelID == channelID), -1)

        if(index>=0):
            self.pairs[index].updateRate(bestBid,bestAsk)

        self.updateMatrix(self.pairs[index])

    def updateMatrix(self, pair):
        upper, lower = pair.pair.split('/')
        upper_index = self.currencies.index(upper)
        lower_index = self.currencies.index(lower)

        if(pair.price != 0):
            self.matrix[upper_index][lower_index] = pair.price
            self.matrix[lower_index][upper_index] = 1/pair.price
        #matprint(self.matrix)
