import sys
sys.path.append("..") # As to import from above
from exchange.exchange import exchange
from operator import itemgetter
import datetime
import os,csv

#Extenstion of exchange model to fit kraken exchange
class kraken_exchange(exchange):

    # REDUNDANT
    def registerTickerChannel(self, channel, pair):
        self.tickerChannels[self.pairs.index(str(pair))]=channel

    #REDUNDANT
    def registerOrderBookChannel(self, channel, pair):
        self.orderBookChannels[self.pairs.index(str(pair))]=channel

    def updateEdgeOrderBook(self, channel, data):
        print(data)

    def updateEdgePrice(self, channel, bestBid, bestAsk):
        print("Update Edge Price")
        pair = self.pairs[self.tickerChannels.index(channel)]
        price = (bestBid+bestAsk)/2
        out,into = pair.split('/',1)
        if price > 0:
            self.graph.remove_edge(out,into)
            self.graph.remove_edge(into,out)
            self.graph.add_edge(out, into, price=float(price), active=True)
            self.graph.add_edge(into, out, price=1/float(price), active=True)
        self.IEAOpportunities()

    def tickerUpdate(self, message):
        #Register channel on connection start
        if 'channelID' in message and 'pair' in message:
            self.registerTickerChannel(int(message.get('channelID')), str(message.get('pair')))
        #Update ticker
        elif(isinstance(message,list)):
            bestAsk = 0
            bestBid = 0
            bestAsk = float(message[1]['a'][0])
            bestBid = float(message[1]['b'][0])
            self.updateEdgePrice(message[0], bestBid, bestAsk)

    def orderBooksUpdate(self, message):
        #Register channel on connection start
        if 'channelID' in message and 'pair' in message:
            self.registerOrderBookChannel(int(message.get('channelID')), str(message.get('pair')))
        elif(isinstance(message,list)):
            order_book = self.orderBooks[self.orderBookChannels.index(int(message[0]))]
            if(order_book == 0):
                order_book = message[1]
            else:
                if('a' in message[1]):
                    asks = order_book['as']
                    emptyAsks = []
                    for update in message[1]['a']:
                        toggleAsks=False
                        for i,order in enumerate(asks):
                            if(float(order[0])==float(update[0])):
                                toggleAsks = True
                                order=update
                                if(float(order[1])==0):
                                    emptyAsks.append(i)
                                break
                        if not toggleAsks:
                            asks.append(update)

                    #Clean up empty ask orders
                    for empty in emptyAsks:
                        try:
                            asks.pop(empty)
                        except:
                            print("ASK POP EXCEPTION", empty)
                        #Sort asks in ascending order
                    asks=sorted(asks, key=itemgetter(0))
                    #Overwrite previous asks
                    self.orderBooks[self.orderBookChannels.index(int(message[0]))]['as'] = asks


                if('b' in message[1]):

                    bids = order_book['bs']
                    emptyBids = []
                    for update in message[1]['b']:
                        toggleBids =False
                        for i,order in enumerate(bids):
                            if(float(order[0])==float(update[0])):
                                toggleBids = True
                                order=update
                                if(float(order[1])==0):
                                    emptyBids.append(i)
                                break
                        if not toggleBids:
                            bids.append(update)

                    #Clean up empty bid orders
                    for empty in emptyBids:
                        try:
                            bids.pop(empty)
                        except:
                            print("BID POP EXCEPTION",empty)

                    #Sort asks in ascending order
                    bids=sorted(bids, key=itemgetter(0), reverse=True)
                    #Overwrite previous asks
                    self.orderBooks[self.orderBookChannels.index(int(message[0]))]['bs'] = bids



            self.orderBooks[self.orderBookChannels.index(int(message[0]))] = order_book


    max = 0

    def IEAOpportunities(self): #Depracated
        for path in self.triplets:
            price_path = []
            price_path.append(self.graph[path[0]][path[1]])
            price_path.append(self.graph[path[1]][path[2]])
            price_path.append(self.graph[path[2]][path[0]])
            #print(price_path)
            toggle = True
            price = 1
            for part in price_path:

                #print(part[0])
                if not part[0]['active']:
                    toggle = False
                    break
                price *= float(part[0]['price'])
            if toggle and price:
                price = price*100-100
                fees = 100-0.9974*0.9974*0.9974*100
                profit = 1* abs(price)*abs(fees)
                if profit > self.max:
                    self.max = profit
                if(profit >0):
                    self.log(path, price, profit, "extra")
                    print("________________________________________________________")
                    upre
                    print(datetime.datetime.now())
                    print(path)
                    print(price,"% profit (Without fees)")
                    print(fees,"% fees")
                    print(profit,"% profit (With fees)")
                    print(path[0],'-',price_path[0][0]['price'],'->',path[1],'-',price_path[1][0]['price'],'->',path[2],price_path[2][0]['price'],'->',path[0])

    def log(self, triplet, price, profit, extra):
        print("Loggingg")
        folderName = "-".join(triplet)
        dt = datetime.datetime.now()
        fileName = dt.strftime('%Y%m%d.log')
        filePath = "logs/"+folderName
        if not os.path.exists(filePath):
            os.makedirs(filePath)
        with open(filePath+"/"+fileName,'a') as logFile:
            writer = csv.writer(logFile,delimiter="|", quoting=csv.QUOTE_MINIMAL)
            log = [dt.strftime('%m/%d/%Y, %H:%M:%S'),triplet, price, profit, extra]
            writer.writerow(log)
