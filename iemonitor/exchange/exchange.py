import matplotlib.pyplot as plt
import networkx as nx
import pickle
import datetime
G=nx.MultiDiGraph()

pairs = ['ADA/CAD', 'ADA/ETH', 'ADA/EUR', 'ADA/USD', 'ADA/XBT', 'BCH/EUR', 'BCH/USD',
    'BCH/XBT', 'BSV/EUR', 'BSV/USD', 'BSV/XBT', 'DASH/EUR', 'DASH/USD', 'DASH/XBT',
    'EOS/ETH', 'EOS/EUR', 'EOS/USD', 'EOS/XBT', 'GNO/ETH', 'GNO/EUR', 'GNO/USD',
    'GNO/XBT', 'QTUM/CAD', 'QTUM/ETH', 'QTUM/EUR', 'QTUM/USD', 'QTUM/XBT', 'USDT/USD',
    'ETC/ETH', 'ETC/XBT', 'ETC/EUR', 'ETC/USD', 'ETH/XBT', 'ETH/CAD', 'ETH/EUR',
    'ETH/GBP', 'ETH/JPY', 'ETH/USD', 'LTC/XBT', 'LTC/EUR', 'LTC/USD', 'MLN/ETH',
    'MLN/XBT', 'REP/ETH', 'REP/XBT', 'REP/EUR', 'REP/USD', 'STR/EUR', 'STR/USD',
    'XBT/CAD', 'XBT/EUR', 'XBT/GBP', 'XBT/JPY', 'XBT/USD', 'BTC/CAD', 'BTC/EUR',
    'BTC/GBP', 'BTC/JPY', 'BTC/USD', 'XDG/XBT', 'XLM/XBT', 'DOGE/XBT', 'STR/XBT',
    'XLM/EUR', 'XLM/USD', 'XMR/XBT', 'XMR/EUR', 'XMR/USD', 'XRP/XBT', 'XRP/CAD',
    'XRP/EUR', 'XRP/JPY', 'XRP/USD', 'ZEC/XBT', 'ZEC/EUR', 'ZEC/JPY', 'ZEC/USD',
    'XTZ/CAD', 'XTZ/ETH', 'XTZ/EUR', 'XTZ/USD', 'XTZ/XBT']



class exchange:
    currencies = []
    channels = [0]*100

    def __init__(self, pairs=pairs):
        for pair in pairs:
            for currency in pair.split('/'):
                if currency not in self.currencies:
                    self.currencies.append(currency)
        self.pairs = pairs
        self.tickerChannels = [0]*len(pairs)
        self.orderBookChannels = [0]*len(pairs)
        self.orderBooks = [0]*len(pairs)


    #Constructing graphs, It might take some time, depending on the edge number
    def constructGraph(self):
        self.graph = nx.MultiDiGraph()
        for pair in self.pairs:
            self.graph.add_edge(pair.split('/')[0],pair.split('/')[1],price=1,active=False)
            self.graph.add_edge(pair.split('/')[1],pair.split('/')[0],price=1,active=False)
        with open('kraken_graph','wb') as file:
            pickle.dump(self.graph, file)
        print("Finding cycles")
        ''' Assuming cycles were already found
        cycles = nx.simple_cycles(self.graph)
        cycles = list(cycles)
        print(len(cycles), " cycles found")
        with open('kaken_cycles','wb') as file:
            pickle.dump(cycles, file)
        print("Cycles were written")
        '''
        file = open("kraken_cycles",'rb')
        cycles = pickle.load(file)
        print("Filtering cycles")
        cycles3 = []
        for cycle in cycles:
            if(len(cycle)==3):
                cycles3.append(cycle)
                print(cycle)

        filtered_cycles = []

        for cycle in cycles3:
            flag = False
            for filtered_cycle in filtered_cycles:
                if sorted(filtered_cycle) == sorted(cycle):
                    flag = True
                    break
            if not flag:
                filtered_cycles.append(cycle)
        with open('kraken_cycles_3','wb') as file:
            pickle.dump(filtered_cycles, file)

        print(filtered_cycles)
        print(len(cycles3))
        print(len(filtered_cycles))


        print(len(list(cycles3)))



    def loadGraph(self, pairs, graph, cycles, triplets):
        graph_file  = open(graph,'rb')
        cycles_file = open(cycles,'rb')
        cycles_3_file = open(triplets, 'rb')
        self.cycles = pickle.load(cycles_file)
        self.triplets = pickle.load(cycles_3_file)
        self.graph = pickle.load(graph_file)


    def updateEdge(self, channel, bestBid, bestAsk):
        pair = self.pairs[self.channels.index(channel)]
        price = (bestBid+bestAsk)/2
        out,into = pair.split('/',1)
        if price > 0:
            self.graph.remove_edge(out,into)
            self.graph.remove_edge(into,out)
            self.graph.add_edge(out, into, price=float(price), active=True)
            self.graph.add_edge(into, out, price=1/float(price), active=True)


        self.IEAOpportunities()
    max = 0 # JUST TO TEST THE WATERS
'''
    def IEAOpportunities(self): #Depracated
        print("Updating")
        for path in self.triplets:
            price_path = []
            price_path.append(self.graph[path[0]][path[1]])
            price_path.append(self.graph[path[1]][path[2]])
            price_path.append(self.graph[path[2]][path[0]])
            toggle = True
            price = 1
            for part in price_path:

                if not part[0]['active']:
                    toggle = False
                    break
                price *= float(part[0]['weight'])
            if toggle and price:
                price = price*100-100
                fees = 100-0.9974*0.9974*0.9974*100
                profit = 1* abs(price)*abs(fees)
                if profit > self.max:
                    self.max = profit
                if(profit >0):
                    print("________________________________________________________")
                    print("max earning", self.max)
                    print(datetime.datetime.now())
                    print(path)
                    print(price,"% profit (Without fees)")
                    print(fees,"% fees")
                    print(profit,"% profit (With fees)")
                    print(path[0],'-',price_path[0][0]['weight'],'->',path[1],'-',price_path[1][0]['weight'],'->',path[2],price_path[2][0]['weight'],'->',path[0])

 
'''
